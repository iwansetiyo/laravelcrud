<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Student;

class MahasiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $mahasiswa = DB::table('students')->get();
        $mahasiswa = Student::all();
        return view('mahasiswa.index', ['mahasiswa' => $mahasiswa]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('mahasiswa.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $student = new Student;

        // $student->nama  = $request->nama;
        // $student->nim   = $request->nim;
        // $student->email = $request->email;
        // $student->jurusan = $request->jurusan;

        // $student->save();

        // Student::create([
        //     'nama' => $request->nama,
        //     'nim' => $request->nim,
        //     'email' => $request->email,
        //     'jurusan' => $request->jurusan
        // ]);

        $request->validate([
            'nama' => 'required',
            'nim' => 'required|size:9'
        ]);

        Student::create($request->all());

        return redirect('mahasiswa')->with('status', 'Data mhs berhasil ditambahkan.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Student $mahasiswa)
    {
        return view('mahasiswa.show', ['student' => $mahasiswa]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Student $mahasiswa)
    {
        return view('mahasiswa.edit', ['student' => $mahasiswa]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Student $mahasiswa)
    {
        $request->validate([
            'nama' => 'required',
            'nim' => 'required|size:9'
        ]);

        Student::where('id', $mahasiswa->id)
                    ->update([
                        'nama' => $request->nama,
                        'nim' => $request->nim,
                        'email' => $request->email,
                        'jurusan' => $request->jurusan
                    ]);
        return redirect('mahasiswa')->with('status', 'Data mhs berhasil diubah.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Student $mahasiswa)
    {
        Student::destroy($mahasiswa->id);
        return redirect('mahasiswa')->with('status', 'Data mhs berhasil dihapus.');
    }
}
