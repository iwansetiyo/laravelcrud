<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;

class StudentController extends Controller
{
    function get()
    {
    	$data = Student::all(['nama', 'nim', 'email', 'jurusan']);

    	return response()->json(
			[
				"messages" => "Success",
				"data" => $data
			]
		);
    }

    function post(Request $request)
    {
    	$student = new Student;

    	$student->nama 	= $request->nama;
    	$student->nim 	= $request->nim;
    	$student->email = $request->email;
    	$student->jurusan = $request->jurusan;

    	$student->save();

    	return response()->json(
			[
				"messages" => "Success",
				"data" => $student
			]
		);
    }

    function put(Request $request, Student $student)
    {
    	Student::where('id', $student->id)
            ->update([
                'nama' => $request->nama,
                'nim' => $request->nim,
                'email' => $request->email,
                'jurusan' => $request->jurusan
            ]);

    	return response()->json(
			[
				"messages" => "Data Updated id:".$student->id
			]
		);
    }

    function delete(Student $student)
    {
    	Student::destroy($student->id);

    	return response()->json(
			[
				"messages" => "Data deleted id:".$student->id
			]
		);
    }
}
