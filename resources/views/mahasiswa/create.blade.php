@extends('layout/main')

@section('title', 'Form Tambah Mahasiswa')

@section('container')
	<div class="container">
		<div class="row">
			<div class="col-10">
				<h1 class="mt-3">Form Tambah Mahasiswa</h1>
				<form method="POST" action="/mahasiswa">
					@csrf
					<div class="form-group">
						<label for="formGroupExampleInput">Nama</label>
						<input type="text" class="form-control @error('nama') is-invalid @enderror" id="nama" placeholder="Masukan nama" name="nama" value="{{ old('nama') }}">
						@error('nama')
						    <div class="alert alert-danger">{{ $message }}</div>
						@enderror
					</div>
					<div class="form-group">
						<label for="formGroupExampleInput">NIM</label>
						<input type="text" class="form-control @error('nim') is-invalid @enderror" id="nim" placeholder="Masukan nim" name="nim" value="{{ old('nim') }}">
						@error('nim')
						    <div class="alert alert-danger">{{ $message }}</div>
						@enderror
					</div>
					<div class="form-group">
						<label for="formGroupExampleInput">Email</label>
						<input type="text" class="form-control @error('email') is-invalid @enderror" id="email" placeholder="Masukan email" name="email" value="{{ old('email') }}">
						@error('email')
						    <div class="alert alert-danger">{{ $message }}</div>
						@enderror
					</div>
					<div class="form-group">
						<label for="formGroupExampleInput">Jurusan</label>
						<input type="text" class="form-control @error('jurusan') is-invalid @enderror" id="jurusan" placeholder="Masukan jurusan" name="jurusan" value="{{ old('jurusan') }}">
						@error('jurusan')
						    <div class="alert alert-danger">{{ $message }}</div>
						@enderror
					</div>

					<button type="submit" class="btn btn-primary">Simpan</button>
				</form>
			</div>
		</div>
	</div>
@endSection