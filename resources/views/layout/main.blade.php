<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

    <title>@yield('title')</title>
  </head>
  <body>
  	<nav class="navbar navbar-expand-lg navbar-light bg-light">
  		<div class="container">
			<a class="navbar-brand" href="#">Iset</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav">
					@if (Route::has('login'))
		                    @if (Auth::check())
								<li class="nav-item active">
									<a class="nav-link" href="{{ url('/') }}">Home</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="{{ url('/about') }}">About</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="{{ url('/mahasiswa') }}">Mahasiswa</a>
								</li>
								<li class="nav-item">
									<form action="{{ url('/logout') }}" method="POST" class="d-inline">
									@method('POST')
									@csrf
										<button type="submit" class="nav-link">Logout</button>
									</form>
								</li>
		                    @else
		                    	<li class="nav-item">
									<a class="nav-link" href="{{ url('/login') }}">Login</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="{{ url('/register') }}">Register</a>
								</li>
		                    @endif
		                </div>
		            @endif
				</ul>
			</div>
		</div>
	</nav>

	@yield('container')

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
  </body>
</html>