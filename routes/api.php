<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('/login', 'AuthController@login');

Route::group([
    'middleware' => ['api','jwt.verify'],
], function ($router) {
    Route::post('/logout', 'AuthController@logout');
    Route::post('/refresh', 'AuthController@refresh');

    // students
	Route::get('/students', 'StudentController@get');

	Route::post('/student', 'StudentController@post');

	Route::put('/student/{student}', 'StudentController@put');

	Route::delete('/student/{student}', 'StudentController@delete');
});
