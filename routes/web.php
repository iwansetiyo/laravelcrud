<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@home');

Route::get('/about', 'PagesController@about')->middleware('auth');

// Route::get('/mahasiswa', 'MahasiswaController@index');
// Route::get('/mahasiswa/create', 'MahasiswaController@create');
// Route::get('/mahasiswa/{student}', 'MahasiswaController@show');
// Route::post('/mahasiswa', 'MahasiswaController@store');

// Route::delete('/mahasiswa/{student}', 'MahasiswaController@destroy');

// Route::get('/mahasiswa/{student}/edit', 'MahasiswaController@edit');
// Route::patch('/mahasiswa/{student}', 'MahasiswaController@update');

Route::resource('mahasiswa', 'MahasiswaController')->middleware('auth');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
